import React, { useState, useEffect } from "react";
import { View, ScrollView, StyleSheet } from "react-native";
import Header from "../components/Header";
import { Title, Body } from "../components/Texts";
import contents from "../constants/contents";
import appStyle from "../components/Styles";

import { IndexPath, Icon } from "@ui-kitten/components";
import colorScheme from "../constants/color-scheme";
import Filter from "../components/call-history/Filter";
import Chart from "../components/call-history/Chart";
import MonthlyTotal from "../components/call-history/MonthlyTotal";
import SeeCreditScore from "../components/call-history/SeeCreditScore";
import HomeButton from "../components/HomeButton";
import Loader from "../components/Loader";

const CallHistory = ({ navigation, route: { params } }) => {
  const phone = params.phone;
  const phoneId = params.phoneId;
  const previousScr = params.previousScr ? params.previousScr : false;
  const [loading, setLoading] = useState(false);
  const [selectedFilter, setSelectedFilter] = useState(0);
  const [monthTotal, setMonthTotal] = useState([]);

  const monthOptions = contents.other.months;
  const yearOptions = contents.other.yearOptions;

  const today = new Date();
  const [year, setYear] = useState(new IndexPath(0));
  const [month, setMonth] = useState(new IndexPath(today.getMonth()));
  //yearOptions[year.row]
  //monthOptions[month.row]

  const defactorCallData = (data) => {
    return {
      callFrequency: data.callFrequency.map((el, index) => ({
        x: index,
        y: el,
      })),
      callingTime: data.callingTime.map((el, index) => ({
        x: index,
        y: el,
      })),
      phoneData: data.phoneData.map((el, index) => ({
        x: index,
        y: el,
      })),
    };
  };
  const [callData, setCallData] = useState([]);
  const chartTitle = {
    callFrequency: {
      title: contents.label.callFrequencyChart,
      unit: "calls",
    },
    callingTime: {
      title: contents.label.callingTimeChart,
      unit: "minutes",
    },
    phoneData: {
      title: contents.label.phoneDataChart,
      unit: "%",
    },
  };

  const fillArray = (ar) => {
    let ar_key = Object.keys(ar);
    let result = [];
    for (let i = 0; i < 31; ++i) {
      result.push(0);
    }
    for (let i = 0; i < ar_key.length; ++i) {
      result[parseInt(ar_key[i].slice(-2)) - 1] = ar[ar_key[i]];
    }
    return result;
  };

  const onFilter = async () => {
    setLoading(true);
    const from = new Date();
    from.setMonth(month);
    from.setDate(1);
    const to = new Date();
    to.setMonth(month);
    fetch(
      `http://eflask-app-devc.herokuapp.com/api/get/chart/${phoneId}?type=${2}&from=${from}&to=${to}`
    )
      .then((json) => json.json())
      .then((response) => {
        let newData = {
          callFrequency: fillArray(JSON.parse(response.count_call).call_time),
          callingTime: fillArray(JSON.parse(response.sum_call).call_time).map(
            (time) => time / 60
          ),
          phoneData: fillArray(JSON.parse(response.data_usage).upload_data),
        };
        let sumDt = newData.phoneData.reduce((pre, cur) => pre + cur);
        if (sumDt > 0)
          newData.phoneData = newData.phoneData.map((dt) => (dt / sumDt) * 100);

        if (selectedFilter === 1) {
          newData.callFrequency = newData.callFrequency.slice(0, 12);
          newData.callingTime = newData.callingTime.slice(0, 12);
          newData.phoneData = newData.phoneData.slice(0, 12);
        }
        setCallData(defactorCallData(newData));
        setLoading(false);
      });
  };

  const formatCalltime = (t) => {
    let h = Math.floor(t / 3600);
    let m = Math.floor((t - h * 3600) / 60);
    let s = Math.floor(t - h * 3600 - m * 60);
    return h + "h " + m + "m " + s + "s";
  };

  const formatData = (d) => {
    const GB = 1073741824;
    const MB = 1048576;
    const KB = 1024;
    if (d > GB) {
      return Number(Math.round(d / GB + "e2") + "e-2") + " GB";
    } else if (d > MB) {
      return Number(Math.round(d / MB + "e2") + "e-2") + " MB";
    } else if (d > KB) {
      return Number(Math.round(d / KB + "e2") + "e-2") + " KB";
    } else return Number(Math.round(d + "e2") + "e-2") + " B";
  };

  const getMonthData = async () => {
    setLoading(true);
    fetch(`http://eflask-app-devc.herokuapp.com/api/get/callhistory/${phoneId}`)
      .then((json) => json.json())
      .then((response) => {
        setMonthTotal([
          {
            label: contents.label.monthlyCall,
            value: response.num_call + " calls",
          },
          {
            label: contents.label.monthlyCallingTime,
            value: formatCalltime(response.total_call_time),
          },
          {
            label: contents.label.monthlyData,
            value: formatData(response.total_data_usage),
          },
        ]);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        console.log(error);
      });
  };

  const onSeeCreditScore = () => {
    if (previousScr) navigation.goBack();
    else
      navigation.push("CreditScore", {
        phone: phone,
        phoneId: phoneId,
        previousScr: true,
      });
  };

  useEffect(() => {
    getMonthData();
    onFilter();
  }, []);

  return (
    <>
      <Loader active={loading} />
      <HomeButton />
      <Header
        title={contents.title.callHistory}
        phonenumber={phone}
        goBack={navigation.goBack}
      />
      <ScrollView style={styles.screen}>
        <View style={styles.container}>
          <MonthlyTotal monthTotal={monthTotal} />
        </View>
        <View style={styles.container}>
          <View style={styles.cardHead}>
            <Icon
              style={appStyle.icon}
              fill={colorScheme.neutral_2}
              name="activity"
            />
            <Body
              content={contents.label.callHistoryChart}
              style={styles.cardTitle}
              color={colorScheme.neutral_0}
              weight="b"
              size={14}
            />
          </View>
          <View style={styles.chartsCard}>
            <Filter
              selectedFilter={selectedFilter}
              setSelectedFilter={setSelectedFilter}
              year={year}
              setYear={setYear}
              month={month}
              setMonth={setMonth}
              onFilter={onFilter}
            />
            {Object.keys(callData).map((section) => (
              <Chart
                key={section}
                title={chartTitle[section].title}
                unit={chartTitle[section].unit}
                data={callData[section]}
              />
            ))}
          </View>
        </View>
        <View style={{ paddingHorizontal: 32, paddingBottom: 72 }}>
          <SeeCreditScore onSeeCreditScore={onSeeCreditScore} />
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  screen: {
    paddingTop: 20,
    paddingBottom: 72,
    backgroundColor: colorScheme.neutral_4,
  },
  cardHead: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 16,
  },
  cardTitle: {
    textTransform: "uppercase",
    paddingLeft: 8,
  },
  container: {
    paddingHorizontal: 32,
  },
  chartsCard: {
    backgroundColor: colorScheme.neutral_5,
    borderRadius: 18,
    marginBottom: 32,
    padding: 18,
  },
});

export default CallHistory;
