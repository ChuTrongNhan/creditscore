import React, { useState, useEffect } from "react";
import { ScrollView, View, StyleSheet, TouchableOpacity } from "react-native";
import colorScheme from "../constants/color-scheme";
import contents from "../constants/contents";
import { ButtonPrimary } from "../components/Buttons";
import Header from "../components/Header";
import CreditScoreCard from "../components/credit-score/CreditScoreCard";
import CreditFactorCard from "../components/credit-score/CreditFactorCard";
import RecentCallCard from "../components/credit-score/RecentCallCard";
import CommunityCard from "../components/credit-score/CommunityCard";
import SeeCallHistory from "../components/credit-score/SeeCallHistory";
import ViewReportButton from "../components/credit-score/ViewReportButton";
import HomeButton from "../components/HomeButton";
import Loader from "../components/Loader";
import { MSISDN, CREDITSCORE, PNUMBER } from "../constants/mapping-id";

const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

const CreditScore = ({ navigation, route: { params } }) => {
  const phone = params.phone;
  const phoneId = params.phoneId;
  const previousScr = params.previousScr ? params.previousScr : false;

  const [loading, setLoading] = useState(false);
  const [viewMore, setViewMore] = useState(false);
  const [creditScoreData, setCreditScoreData] = useState({
    credit_score: 0,
    credit_score_last_month: 0,
    credit_score_last_week: 0,
    top: 0,
  });
  const [userDetailData, setUserDetailData] = useState({});

  const parseCommunityArray = (ar) => {
    let result = [];
    let sum = ar.reduce((pre, cur) => ({ count: pre.count + cur.count }));
    if (sum.count === 0) sum.count = 1;
    for (let i = 0; i < ar.length; ++i) {
      result.push({ x: ar[i].label, y: (ar[i].count / sum.count) * 100 });
    }
    return result;
  };

  const retrieveCreditScore = async () => {
    setLoading(true);
    fetch(`http://eflask-app-devc.herokuapp.com/api/get/creditscore/${phoneId}`)
      .then((json) => json.json())
      .then((response) => {
        setCreditScoreData({
          credit_score: Math.round(response.credit_score),
          credit_score_last_month: Math.round(response.credit_score_last_month),
          credit_score_last_week: Math.round(response.credit_score_last_week),
          top: Number(Math.round(response.top + "e5") + "e-3"),
        });
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        console.log(error);
      });
  };

  const getRandomPhoneId = () => Math.floor(Math.random() * MSISDN.length);

  const showDetail = async () => {
    setLoading(true);
    fetch(`http://eflask-app-devc.herokuapp.com/api/get/info/${phoneId}`)
      .then((json) => json.json())
      .then((response) => {
        let recentCalltmp = [];
        for (let i = 0; i < 10; ++i) {
          let r_id = getRandomPhoneId();
          recentCalltmp.push({
            phone: PNUMBER[i],
            phoneId: MSISDN[r_id],
            credit_score: CREDITSCORE[r_id] ? Math.round(CREDITSCORE[r_id]) : 0,
          });
        }
        setUserDetailData({
          recentCalls: recentCalltmp,
          factorList: [
            {
              label: contents.label.payments,
              value: numberWithCommas(Math.round(response.total_pay)),
            },
            {
              label: contents.label.deposit,
              value: numberWithCommas(Math.round(response.recharge)),
            },
            {
              label: contents.label.debt,
              value: numberWithCommas(Math.round(response.total_loan)),
            },
          ],
          info: {
            age: response.customer_info.age,
            gender: response.customer_info.gender,
            province: response.customer_info.province,
            telephone: phone,
            signup_date: new Date(
              response.customer_info.signup_date
            ).toDateString(),
            credit_score: creditScoreData.credit_score,
          },
          community: parseCommunityArray(JSON.parse(response.community)),
        });
        setViewMore(true);
        setLoading(false);
      })
      .catch((error) => {
        setLoading(false);
        console.log(error, phoneId);
      });
  };

  const seeDetail = (call) => {
    if (call.credit_score > 0)
      navigation.push("CreditScore", {
        phone: call.phone,
        phoneId: call.phoneId,
      });
  };

  const seeCallHistory = () => {
    if (previousScr) navigation.goBack();
    else
      navigation.push("CallHistory", {
        phone: phone,
        phoneId: phoneId,
        previousScr: true,
      });
  };

  const viewReport = () => {
    navigation.push("CreditReport", { info: userDetailData.info });
  };

  useEffect(() => {
    retrieveCreditScore();
  }, []);

  return (
    <>
      <Loader active={loading} />
      <HomeButton />
      <Header
        title={contents.title.creditScore}
        phonenumber={phone}
        goBack={navigation.goBack}
      />
      <ScrollView style={styles.screen}>
        <View style={styles.container}>
          <CreditScoreCard
            score={creditScoreData.credit_score}
            top={creditScoreData.top}
            lastMonth={creditScoreData.credit_score_last_month}
            lastWeek={creditScoreData.credit_score_last_week}
          />
          {viewMore ? (
            <>
              <CreditFactorCard factorList={userDetailData.factorList} />
              <RecentCallCard
                recentCalls={userDetailData.recentCalls}
                onSeeDetail={seeDetail}
              />
              <CommunityCard community={userDetailData.community} />
              <ViewReportButton onViewReport={viewReport} />
              <SeeCallHistory onSeeCallHistory={seeCallHistory} />
            </>
          ) : (
            <ButtonPrimary
              label={contents.label.moreDetail}
              onPress={showDetail}
            />
          )}
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  screen: {
    paddingTop: 20,
    paddingBottom: 40,
    backgroundColor: colorScheme.neutral_4,
  },
  container: {
    paddingHorizontal: 32,
    paddingBottom: 72,
  },
});

export default CreditScore;
