import React, { useState, useEffect } from "react";
import {
  View,
  Keyboard,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  AsyncStorage,
} from "react-native";
import { Icon } from "@ui-kitten/components";
import { CheckBox, Modal, Card } from "@ui-kitten/components";

import contents from "../constants/contents";
import colorScheme from "../constants/color-scheme";

import appStyle from "../components/Styles";
import { Title, Body } from "../components/Texts";
import { ButtonPrimary } from "../components/Buttons";
import { Input } from "../components/TextInput";
import LoginFailAlert from "../components/login/LoginFailAlert";

const Login = ({ navigation, route: { params } }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [securePassword, setSecurePassword] = useState(true);

  const [checked, setChecked] = useState(false);
  const [loginFail, setLoginFail] = useState(false);

  const storeEmail = async () => {
    try {
      await AsyncStorage.setItem("@email", email);
    } catch (error) {
      console.log("Error:", error);
    }
  };

  retrieveEmail = async () => {
    try {
      const value = await AsyncStorage.getItem("@email");
      if (value !== null) {
        setEmail(value);
      }
    } catch (error) {
      console.log("Error:", error);
    }
  };

  const login = () => {
    if (password === "123456") {
      if (checked) storeEmail();
      setPassword("");
      setSecurePassword(true);
      navigation.navigate("Home");
    } else {
      setLoginFail(true);
    }
  };

  useEffect(() => {
    retrieveEmail();
  }, []);

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={styles.screen}>
        <Title
          color={colorScheme.primary}
          content={contents.title.login}
          style={{ marginBottom: 32 }}
        />
        <Modal
          visible={loginFail}
          backdropStyle={styles.backdrop}
          onBackdropPress={() => setLoginFail(false)}
        >
          <LoginFailAlert
            status={email == "" || password == "" ? 1 : 0}
            onCancel={() => setLoginFail(false)}
          />
        </Modal>

        <View style={styles.textField}>
          <Input
            keyboardType="email-address"
            placeholder={contents.label.email}
            value={email}
            onChangeText={setEmail}
          />
        </View>
        <View style={styles.textField}>
          <Input
            placeholder={contents.label.password}
            value={password}
            secure={securePassword}
            onChangeText={setPassword}
            style={{ paddingRight: 42 }}
          />
          <TouchableOpacity
            style={{ ...appStyle.iconInputRight }}
            onPress={() => setSecurePassword(!securePassword)}
          >
            {securePassword ? (
              <Icon
                style={appStyle.icon}
                fill={colorScheme.primary}
                name="eye-off-outline"
              />
            ) : (
              <Icon
                style={appStyle.icon}
                fill={colorScheme.primary}
                name="eye-outline"
              />
            )}
          </TouchableOpacity>
        </View>
        <View style={{ width: "100%" }}>
          <CheckBox
            style={{ marginBottom: 36 }}
            checked={checked}
            onChange={(nextChecked) => setChecked(nextChecked)}
          >
            <Body
              content={contents.other.rememberLogin}
              color={colorScheme.neutral_1}
            />
          </CheckBox>
        </View>
        <ButtonPrimary label={contents.label.login} onPress={() => login()} />
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: colorScheme.neutral_5,
    justifyContent: "center",
    alignItems: "center",
    padding: "10%",
  },
  textField: {
    marginBottom: 18,
    width: "100%",
  },
  backdrop: {
    backgroundColor: colorScheme.backdrop,
  },
});

export default Login;
