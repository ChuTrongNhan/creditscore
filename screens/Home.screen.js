import React, { useState, useRef } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import { Icon, Modal } from "@ui-kitten/components";
import appStyle from "../components/Styles";
import colorScheme from "../constants/color-scheme";
import contents from "../constants/contents";
import layout from "../constants/Layout";
import { Body, Title } from "../components/Texts";
import * as Animatable from "react-native-animatable";

import Tabbar2 from "../components/Tabbar2";
import { InputPhoneNumber } from "../components/TextInput";
import { ButtonPrimary } from "../components/Buttons";
import Loader from "../components/Loader";
import LogoutAlert from "../components/home/LogoutAlert";
import NotFoundAlert from "../components/home/NotFoundAlert";

import { MSISDN } from "../constants/mapping-id";

const Header = ({ onLogout }) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity style={styles.headerIcon} onPress={onLogout}>
        <Icon
          style={appStyle.iconLarge}
          fill={colorScheme.neutral_2}
          name="person-outline"
        />
      </TouchableOpacity>
    </View>
  );
};

const Home = ({ navigation, route: { params } }) => {
  const [tab, setTab] = useState(true);
  const [phone, setPhone] = useState("");
  const [loading, setLoading] = useState(false);
  const [showLogoutModal, setShowLogoutModal] = useState(false);
  const [showNotFoundModal, setShowNotFoundModal] = useState(false);

  const creditScoreRef = useRef(null);
  const callHistoryRef = useRef(null);

  const animationTransitionTime = 400;

  const onSelectTab = (selectTab) => {
    if (selectTab) {
      creditScoreRef.current.animate("fadeInUpBig", animationTransitionTime);
      callHistoryRef.current.animate("fadeOutDownBig", animationTransitionTime);
    } else {
      creditScoreRef.current.animate("fadeOutDownBig", animationTransitionTime);
      callHistoryRef.current.animate("fadeInUpBig", animationTransitionTime);
    }
    setTab(selectTab);
  };

  const formatPhone = (phonenumber) => {
    let i = phonenumber.length;
    let formatedPhone = "";
    while (i--) {
      const ch = phonenumber.charAt(i);
      if (ch >= "0" && ch <= "9") formatedPhone = ch + formatedPhone;
    }
    if (formatedPhone.length > 10)
      formatedPhone = formatedPhone
        .slice(0, 10)
        .replace(/([0-9]{3})([0-9]{3})/g, `($1) $2-`);
    else if (formatedPhone.length > 6)
      formatedPhone = formatedPhone.replace(
        /([0-9]{3})([0-9]{3})/g,
        `($1) $2-`
      );
    else if (formatedPhone.length > 3)
      formatedPhone = formatedPhone.replace(/(^[0-9]{3})/g, `($1) `);
    setPhone(formatedPhone);
  };

  const getPhoneId = () => {
    if (phone.length < 14) return "";
    else return MSISDN[Math.floor(Math.random() * MSISDN.length)];
  };

  const searchCreditScore = () => {
    const phoneId = getPhoneId();
    if (phoneId !== "")
      navigation.navigate("CreditScore", { phone: phone, phoneId: phoneId });
    else setShowNotFoundModal(true);
  };

  const searchCallHistory = () => {
    const phoneId = getPhoneId();
    if (phoneId !== "")
      navigation.navigate("CallHistory", { phone: phone, phoneId: phoneId });
    else setShowNotFoundModal(true);
  };

  const onLogout = () => {
    navigation.navigate("Login");
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={styles.screen}>
        <Loader active={loading} />
        <Header onLogout={() => setShowLogoutModal(true)} />
        <Modal
          visible={showLogoutModal}
          backdropStyle={styles.backdrop}
          onBackdropPress={() => setShowLogoutModal(false)}
        >
          <LogoutAlert
            onOK={onLogout}
            onCancel={() => setShowLogoutModal(false)}
          />
        </Modal>
        <Modal
          visible={showNotFoundModal}
          backdropStyle={styles.backdrop}
          onBackdropPress={() => setShowNotFoundModal(false)}
        >
          <NotFoundAlert onCancel={() => setShowNotFoundModal(false)} />
        </Modal>
        <Tabbar2
          status={tab}
          firstLabel={contents.label.creditScore}
          secondLabel={contents.label.callHistory}
          onSelect={(selectTab) => onSelectTab(selectTab)}
        />
        <View
          style={{ ...styles.backgroundCard, ...appStyle.softShadow }}
        ></View>
        <Animatable.View
          ref={creditScoreRef}
          style={{ ...styles.card, ...appStyle.softShadow }}
        >
          <Title
            content={contents.title.creditScore}
            style={{ marginBottom: 24 }}
          />
          <Body
            content={contents.label.customerPhoneNumber}
            color={colorScheme.neutral_2}
          />
          <InputPhoneNumber
            value={phone}
            onChangeText={formatPhone}
            style={{ marginVertical: 18 }}
          />
          <ButtonPrimary
            label={contents.label.searchCallHistory}
            onPress={searchCreditScore}
          />
        </Animatable.View>

        <Animatable.View
          ref={callHistoryRef}
          style={{
            ...styles.card,
            ...appStyle.softShadow,
            transform: [
              {
                translateX: -layout.window.height,
              },
            ],
          }}
        >
          <Title
            content={contents.title.callHistory}
            style={{ marginBottom: 24 }}
          />
          <Body
            content={contents.label.customerPhoneNumber}
            color={colorScheme.neutral_2}
          />
          <InputPhoneNumber
            value={phone}
            onChangeText={formatPhone}
            style={{ marginVertical: 18 }}
          />
          <ButtonPrimary
            label={contents.label.searchCreditScore}
            onPress={searchCallHistory}
          />
        </Animatable.View>
      </View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    paddingTop: 20,
    backgroundColor: colorScheme.neutral_4,
  },
  header: {
    padding: 18,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundColor: colorScheme.neutral_4,
  },
  headerIcon: {
    height: 42,
    width: 42,
    borderWidth: 2,
    borderColor: colorScheme.primary,
    borderRadius: 42,
    alignItems: "center",
    justifyContent: "center",
  },
  backgroundCard: {
    backgroundColor: colorScheme.neutral_5,
    opacity: 0.75,
    width: "80%",
    height: layout.window.height - 180,
    marginHorizontal: "10%",
    position: "absolute",
    bottom: 0,
    borderRadius: 18,
  },
  card: {
    backgroundColor: colorScheme.neutral_5,
    height: layout.window.height - 200,
    width: "100%",
    position: "absolute",
    bottom: 0,
    borderRadius: 18,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    alignItems: "center",
    padding: 32,
  },

  backdrop: {
    backgroundColor: colorScheme.backdrop,
  },
});

export default Home;
