import React, { useState } from "react";
import { View, ScrollView, StyleSheet, TouchableOpacity } from "react-native";
import { Icon } from "@ui-kitten/components";
import Header from "../components/Header";
import contents from "../constants/contents";
import colorScheme from "../constants/color-scheme";
import { Body, Title } from "../components/Texts";
import appStyle from "../components/Styles";
import HomeButton from "../components/HomeButton";

const CardTitle = ({ content }) => {
  return (
    <View>
      <Body
        content={content}
        size={14}
        weight="b"
        color={colorScheme.neutral_2}
      />
    </View>
  );
};

const InfoRow = ({ label, value }) => {
  return (
    <View style={styles.infoRow}>
      <Body
        content={label}
        size={14}
        weight="m"
        color={colorScheme.neutral_2}
      />
      <Body content={value} size={14} />
    </View>
  );
};

const EvaluationTable = () => {
  const [evaluationShow, setEvaluationShow] = useState(false);
  const evaluations = [
    { score: "300-579", rank: contents.other.poorCreditScore },
    { score: "580-669", rank: contents.other.fairCreditScore },
    { score: "670-739", rank: contents.other.goodCreditScore },
    { score: "740-799", rank: contents.other.verygoodCreditScore },
    { score: "800-850", rank: contents.other.exceptionalCreditScore },
  ];
  return (
    <View style={styles.evaluationTable}>
      <TouchableOpacity
        style={{ flexDirection: "row", alignItems: "center" }}
        onPress={() => setEvaluationShow(!evaluationShow)}
      >
        <Body
          content={contents.label.creditScoreEvaluation}
          color={colorScheme.neutral_2}
          size={14}
        />
        {evaluationShow ? (
          <Icon
            style={appStyle.icon}
            fill={colorScheme.neutral_2}
            name="chevron-down"
          />
        ) : (
          <Icon
            style={appStyle.icon}
            fill={colorScheme.neutral_2}
            name="chevron-right"
          />
        )}
      </TouchableOpacity>
      {evaluationShow
        ? evaluations.map((evaluation) => (
            <Body
              key={evaluation.rank}
              content={evaluation.score + " - " + evaluation.rank}
              color={colorScheme.neutral_2}
              size={12}
            />
          ))
        : null}
    </View>
  );
};

const CreditReport = ({ navigation, route: { params } }) => {
  const info = params.info;
  const today = new Date();
  const getRank = (score) => {
    if (score > 799) return contents.other.exceptionalCreditScore;
    if (score > 739) return contents.other.verygoodCreditScore;
    if (score > 669) return contents.other.goodCreditScore;
    if (score > 579) return contents.other.fairCreditScore;
    return contents.other.poorCreditScore;
  };
  return (
    <>
      <HomeButton />
      <Header title={contents.title.creditReport} goBack={navigation.goBack} />
      <ScrollView style={styles.screen}>
        <View style={styles.container}>
          <View style={{ ...styles.card, ...styles.vertical }}>
            <CardTitle content={contents.label.reportDate} />
            <Body content={today.toDateString()} size={14} />
          </View>
          <View style={styles.cardHead}>
            <Icon
              style={appStyle.icon}
              fill={colorScheme.neutral_2}
              name="info-outline"
            />
            <Body
              content={contents.label.creditorInfo}
              style={styles.cardTitle}
              color={colorScheme.neutral_0}
              weight="b"
            />
          </View>
          <View style={styles.card}>
            <InfoRow
              label={contents.label.creditorGender}
              value={
                info.gender == "M"
                  ? "Male"
                  : info.gender == "F"
                  ? "Female"
                  : "no data"
              }
            />
            <InfoRow
              label={contents.label.creditorAge}
              value={info.age > 0 ? info.age : "no data"}
            />
            <InfoRow
              label={contents.label.creditorAddress}
              value={
                info.province && info.province !== " "
                  ? info.province
                  : "no data"
              }
            />
            <InfoRow
              label={contents.label.creditorPhone}
              value={info.telephone}
            />
            <InfoRow
              label={contents.label.creditorSignupDate}
              value={info.signup_date}
            />
          </View>
          <View style={{ ...styles.card, ...styles.vertical }}>
            <CardTitle content={contents.label.creditScore} />
            <View style={{ justifyContent: "center", alignItems: "flex-end" }}>
              <Body
                content={info.credit_score}
                color={colorScheme.primary}
                size={28}
                weight="b"
              />
              <Body
                content={getRank(info.credit_score) + " SCORE"}
                size={14}
                weight="b"
              />
            </View>
          </View>
          <EvaluationTable />
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  screen: {
    paddingTop: 20,
    paddingBottom: 40,
    backgroundColor: colorScheme.neutral_4,
  },
  container: {
    paddingHorizontal: 32,
    paddingBottom: 72,
  },
  cardHead: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 8,
  },
  cardTitle: {
    textTransform: "uppercase",
    paddingLeft: 8,
  },
  card: {
    borderRadius: 8,
    backgroundColor: colorScheme.neutral_5,
    padding: 18,
    marginBottom: 18,
  },
  vertical: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  infoRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 4,
  },
  evaluationTable: {
    alignItems: "center",
  },
});

export default CreditReport;
