import React from "react";
import { StyleSheet } from "react-native";

import colorScheme from "../constants/color-scheme";

const commonBorderRadius = 8;
const buttonHeight = 42;

const appStyle = StyleSheet.create({
  textInput: {
    backgroundColor: colorScheme.neutral_4,
    fontFamily: "gs-r",
    fontSize: 16,
    color: colorScheme.neutral_0,
    borderRadius: commonBorderRadius,
    paddingHorizontal: 16,
    paddingVertical: 10,
    width: "100%",
    height: 56,
    borderBottomWidth: 2,
    borderBottomColor: "rgba(0,0,0,0)",
  },
  phoneInput: {
    backgroundColor: colorScheme.neutral_4,
    fontFamily: "gs-m",
    fontSize: 24,
    color: colorScheme.neutral_0,
    borderRadius: commonBorderRadius,
    paddingHorizontal: 16,
    paddingVertical: 10,
    width: "100%",
    height: 56,
    borderBottomWidth: 2,
    borderBottomColor: "rgba(0,0,0,0)",
    textAlign: "center",
  },
  textInputFocus: {
    borderBottomColor: colorScheme.primary,
  },

  icon: { width: 24, height: 24 },
  iconLarge: { width: 32, height: 32 },
  iconSmall: { width: 18, height: 18 },
  iconInputRight: {
    position: "absolute",
    right: 0,
    height: 56,
    width: 42,
    justifyContent: "center",
    alignItems: "center",
  },

  buttonPrimary: {
    height: buttonHeight,
    backgroundColor: colorScheme.accent,
    borderRadius: commonBorderRadius,
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 18,
    minWidth: "100%",
  },
  buttonPrimaryLabel: {
    fontSize: 16,
    fontFamily: "gs-m",
    color: colorScheme.neutral_5,
  },
  buttonIcon: {
    height: buttonHeight,
    width: buttonHeight,
    backgroundColor: colorScheme.neutral_5,
    borderRadius: 8,
    borderColor: colorScheme.accent,
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
  },

  softShadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.12,
    shadowRadius: 24.0,

    elevation: 24,
  },
});

export default appStyle;
