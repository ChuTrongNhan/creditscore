import React, { useState } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Body } from "../Texts";
import {
  Icon,
  Select,
  SelectItem,
  Radio,
  RadioGroup,
} from "@ui-kitten/components";
import contents from "../../constants/contents";

import { ButtonPrimary } from "../Buttons";
import colorScheme from "../../constants/color-scheme";
import appStyle from "../Styles";

const Filter = ({
  selectedFilter,
  setSelectedFilter,
  month,
  setMonth,
  year,
  setYear,
  onFilter,
}) => {
  const [visible, setVisible] = useState(true);
  const filterOptions = [contents.label.filterMonth, contents.label.filterYear];
  const monthOptions = contents.other.months;
  const yearOptions = contents.other.yearOptions;
  return (
    <View style={styles.filter}>
      <TouchableOpacity
        style={styles.selectionBoxTitle}
        onPress={() => setVisible(!visible)}
      >
        {visible ? (
          <Icon
            style={appStyle.iconSmall}
            fill={colorScheme.primary}
            name="arrow-down"
          />
        ) : (
          <Icon
            style={appStyle.iconSmall}
            fill={colorScheme.primary}
            name="arrow-up"
          />
        )}

        <Body
          style={{ paddingLeft: 8 }}
          content={contents.other.openFilterCallHistory}
          color={colorScheme.primaryDarker}
          weight="b"
        />
      </TouchableOpacity>
      {visible ? (
        <View style={styles.filterSelects}>
          <RadioGroup
            style={styles.radioGroup}
            selectedIndex={selectedFilter}
            onChange={(index) => setSelectedFilter(index)}
          >
            {filterOptions.map((opt) => (
              <Radio key={opt}>{opt}</Radio>
            ))}
          </RadioGroup>
          {selectedFilter == 0 ? (
            <Select
              style={{ marginBottom: 8 }}
              value={monthOptions[month.row]}
              selectedIndex={month}
              onSelect={(index) => setMonth(index)}
            >
              {monthOptions.map((monthOption) => (
                <SelectItem key={monthOption} title={monthOption} />
              ))}
            </Select>
          ) : null}
          <Select
            value={yearOptions[year.row]}
            selectedIndex={year}
            onSelect={(index) => setYear(index)}
          >
            {yearOptions.map((yearOption) => (
              <SelectItem key={yearOption} title={yearOption} />
            ))}
          </Select>

          <View style={{ marginTop: 24 }}>
            <TouchableOpacity style={styles.filterButton} onPress={onFilter}>
              <Body
                content={contents.label.filter}
                color={colorScheme.primaryDarker}
                weight="m"
                style={{ paddingRight: 8 }}
              />
              <Icon
                style={appStyle.iconSmall}
                fill={colorScheme.primaryDarker}
                name="corner-down-right"
              />
            </TouchableOpacity>
          </View>
        </View>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  filter: {
    backgroundColor: colorScheme.neutral_5,
    marginBottom: 18,
  },
  filterSelects: {},
  selectionBoxTitle: {
    flexDirection: "row",
    alignItems: "center",
  },
  radioGroup: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 18,
  },
  filterButton: {
    flexDirection: "row",
    borderRadius: 8,
    height: 42,
    paddingHorizontal: 18,
    alignItems: "center",
    justifyContent: "center",
    borderColor: colorScheme.primary,
    borderWidth: 1,
  },
});

export default Filter;
