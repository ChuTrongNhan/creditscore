import React from "react";
import { StyleSheet, View } from "react-native";
import { VictoryChart, VictoryArea } from "victory-native";
import { Body } from "../Texts";
import colorScheme from "../../constants/color-scheme";
import theme from "./ChartTheme";
import { Icon } from "@ui-kitten/components";
import appStyle from "../Styles";
import layout from "../../constants/Layout";
import contents from "../../constants/contents";

const Chart = ({ title, unit, data }) => {
  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <View style={styles.chartTitle}>
          <Body content={title} weight="r" color={colorScheme.neutral_1} />
          <Body
            content={"(" + unit + ")"}
            size={12}
            color={colorScheme.neutral_2}
          />
        </View>
        {data && data.length > 0 ? (
          <VictoryChart width={layout.window.width - 32 * 2} theme={theme}>
            <VictoryArea
              interpolation="natural"
              style={{
                data: {
                  stroke: colorScheme.primary,
                  strokeWidth: 1.5,
                  fill: "rgba(9, 202, 186, 0)",
                },
                parent: { border: colorScheme.neutral_1 },
              }}
              data={data}
            />
          </VictoryChart>
        ) : (
          <View style={{ alignItems: "center" }}>
            <Icon
              style={appStyle.iconSmall}
              fill={colorScheme.neutral_3}
              name="alert-circle"
            />
            <Body
              content={contents.other.noCallHistoryChartData}
              size={14}
              color={colorScheme.neutral_2}
              style={{ textAlign: "center", marginTop: 8 }}
            />
          </View>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: 32,
  },
  chartTitle: {
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 32,
    paddingBottom: 8,
    borderTopColor: colorScheme.neutral_4,
    borderTopWidth: 2,
  },
});

export default Chart;
