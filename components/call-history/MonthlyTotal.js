import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { Body } from "../Texts";
import contents from "../../constants/contents";
import { Icon } from "@ui-kitten/components";
import colorScheme from "../../constants/color-scheme";
import appStyle from "../Styles";

const FactorRow = ({ style, factor, children }) => {
  return (
    <View style={style}>
      <View style={styles.factorRow}>
        <Body
          style={{ maxWidth: "50%" }}
          content={factor.label}
          color={colorScheme.neutral_1}
          size={14}
          weight="m"
        />
        <Body
          style={{ maxWidth: "50%", textAlign: "right" }}
          content={factor.value}
          color={colorScheme.primaryDarker}
          size={14}
          weight="b"
        />
      </View>
      {children}
    </View>
  );
};

const MonthlyTotal = ({ monthTotal = [] }) => {
  return (
    <>
      <View style={styles.cardHead}>
        <Icon
          style={appStyle.icon}
          fill={colorScheme.neutral_2}
          name="calendar"
        />
        <Body
          content={contents.label.callHistoryThisMonth}
          style={styles.cardTitle}
          color={colorScheme.neutral_0}
          weight="b"
          size={14}
        />
      </View>
      <View style={styles.card}>
        {monthTotal.map((factor, index) => (
          <FactorRow
            key={factor.label}
            factor={factor}
            style={
              index > 0 && index < monthTotal.length - 1
                ? styles.borderVertical
                : {}
            }
          />
        ))}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  card: {
    backgroundColor: colorScheme.neutral_5,
    borderRadius: 18,
    paddingHorizontal: 24,
    paddingVertical: 8,
    marginBottom: 36,
  },
  cardHead: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 16,
  },
  cardTitle: {
    paddingLeft: 8,
    textTransform: "uppercase",
  },
  factorRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 18,
    alignItems: "center",
  },
  borderVertical: {
    borderTopColor: colorScheme.neutral_4,
    borderTopWidth: 1,
    borderBottomColor: colorScheme.neutral_4,
    borderBottomWidth: 1,
  },
});

export default MonthlyTotal;
