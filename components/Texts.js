import React from "react";
import { Text } from "react-native";
import colorScheme from "../constants/color-scheme";

export const TitleScreen = ({ color, content, style }) => {
  return (
    <Text
      style={{
        color: color ? color : colorScheme.neutral_0,
        fontSize: 28,
        fontFamily: "gs-b",
        ...style,
      }}
    >
      {content}
    </Text>
  );
};

export const Title = ({ color, content, style }) => {
  return (
    <Text
      style={{
        color: color ? color : colorScheme.neutral_0,
        fontSize: 36,
        fontFamily: "gs-b",
        ...style,
      }}
    >
      {content}
    </Text>
  );
};

export const Body = ({ color, size, weight, content, style }) => {
  return (
    <Text
      style={{
        color: color ? color : colorScheme.neutral_0,
        fontSize: size ? size : 16,
        fontFamily: weight ? "gs-" + weight : "gs-r",
        ...style,
      }}
    >
      {content}
    </Text>
  );
};
