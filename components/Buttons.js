import React from "react";
import { Text, TouchableOpacity } from "react-native";
import appStyle from "../components/Styles";
import colorScheme from "../constants/color-scheme";
import { Icon } from "@ui-kitten/components";

export const ButtonPrimary = ({ label, onPress, disabled }) => {
  return (
    <TouchableOpacity
      style={appStyle.buttonPrimary}
      activeOpacity={disabled ? 1 : 0.8}
      onPress={disabled ? () => {} : onPress}
    >
      <Text style={appStyle.buttonPrimaryLabel}>{label}</Text>
    </TouchableOpacity>
  );
};

export const ButtonIcon = ({ iconName, onPress, disabled }) => {
  return (
    <TouchableOpacity
      style={appStyle.buttonIcon}
      activeOpacity={disabled ? 1 : 0.8}
      onPress={disabled ? () => {} : onPress}
    >
      <Icon style={appStyle.icon} fill={colorScheme.accent} name={iconName} />
    </TouchableOpacity>
  );
};
