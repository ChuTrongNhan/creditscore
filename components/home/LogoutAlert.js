import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Card, Icon } from "@ui-kitten/components";
import { Body } from "../Texts";
import contents from "../../constants/contents";
import colorScheme from "../../constants/color-scheme";

const LogoutAlert = ({ onOK, onCancel }) => {
  const Footer = () => {
    return (
      <View style={styles.footer}>
        <TouchableOpacity style={styles.footButton} onPress={onCancel}>
          <Body
            content={contents.label.notLogout}
            size={16}
            weight="m"
            color={colorScheme.neutral_2}
          />
        </TouchableOpacity>

        <TouchableOpacity style={styles.footButton} onPress={onOK}>
          <Body
            content={contents.label.doLogout}
            size={16}
            weight="b"
            color={colorScheme.primary}
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <Card footer={Footer}>
      <View style={styles.body}>
        <Icon
          style={{ width: 28, height: 28 }}
          fill={colorScheme.primary}
          name="alert-circle-outline"
        />
        <Body
          content={contents.other.logoutAlert}
          color={colorScheme.neutral_1}
          style={{ textAlign: "center", marginTop: 8 }}
        />
      </View>
    </Card>
  );
};

const styles = StyleSheet.create({
  footer: {
    flexDirection: "row",
    paddingVertical: 18,
    alignItems: "center",
    justifyContent: "space-around",
  },
  footButton: {
    paddingHorizontal: 32,
    alignItems: "center",
    justifyContent: "center",
  },
  body: {
    padding: 8,
    alignItems: "center",
  },
});

export default LogoutAlert;
