import React, { useRef } from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import colorScheme from "../constants/color-scheme";
import layout from "../constants/Layout";
import { Body } from "../components/Texts";
import * as Animatable from "react-native-animatable";

const Tabbar2 = ({ status, firstLabel, secondLabel, onSelect }) => {
  const tabIndicator = useRef(null);

  const moveToRight = {
    from: {
      translateX: 18,
    },
    to: {
      translateX: layout.window.width / 2 - 18,
    },
  };

  const moveToLeft = {
    from: {
      translateX: layout.window.width / 2 - 18,
    },
    to: {
      translateX: 0,
    },
  };

  return (
    <View style={styles.tabbar}>
      <View style={styles.tabContainer}>
        <TouchableOpacity
          style={styles.tab}
          onPress={() => {
            if (!status) {
              tabIndicator.current.animate(moveToLeft, 200);
              onSelect(true);
            }
          }}
        >
          <Body
            color={status ? colorScheme.primaryDarker : colorScheme.neutral_2}
            weight="b"
            size={18}
            content={firstLabel}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.tab}
          onPress={() => {
            if (status) {
              tabIndicator.current.animate(moveToRight, 200);
              onSelect(false);
            }
          }}
        >
          <Body
            color={!status ? colorScheme.primaryDarker : colorScheme.neutral_2}
            weight="b"
            size={18}
            content={secondLabel}
          />
        </TouchableOpacity>
      </View>
      <Animatable.View ref={tabIndicator} style={styles.tab} easing="ease-out">
        <View style={styles.tabIndicator}></View>
      </Animatable.View>
    </View>
  );
};

const styles = StyleSheet.create({
  tabbar: {
    padding: 18,
  },
  tabContainer: {
    flexDirection: "row",
  },
  tab: {
    width: "50%",
    alignItems: "center",
  },
  tabIndicator: {
    margin: 6,
    width: 8,
    height: 8,
    borderRadius: 8,
    backgroundColor: colorScheme.primary,
  },
});

export default Tabbar2;
