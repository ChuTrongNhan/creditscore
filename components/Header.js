import React from "react";
import { View, TouchableOpacity, StyleSheet } from "react-native";
import appStyle from "./Styles";
import { Icon } from "@ui-kitten/components";
import colorScheme from "../constants/color-scheme";
import { Body, TitleScreen } from "../components/Texts";

const Header = ({ title, phonenumber, goBack }) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity style={styles.headerIcon} onPress={() => goBack()}>
        <Icon
          style={appStyle.iconLarge}
          fill={colorScheme.neutral_2}
          name="arrow-ios-back-outline"
        />
      </TouchableOpacity>
      <View style={{ paddingLeft: 8 }}>
        <TitleScreen content={title} />
        {phonenumber ? (
          <View style={styles.subTitle}>
            <Icon
              style={appStyle.iconSmall}
              fill={colorScheme.neutral_3}
              name="person"
            />
            <Body
              content={phonenumber}
              size={14}
              color={colorScheme.neutral_1}
              style={{ padding: 4 }}
            />
          </View>
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 18,
    paddingTop: 32,
    paddingBottom: 8,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: colorScheme.neutral_4,
  },
  headerIcon: {
    height: 42,
    width: 42,
    alignItems: "center",
    justifyContent: "center",
  },
  subTitle: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export default Header;
