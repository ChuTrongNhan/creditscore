import React, { useState } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import * as RootNavigation from "../navigation/RootNavigation";
import { SimpleLineIcons } from "@expo/vector-icons";
import colorScheme from "../constants/color-scheme";
import { Body } from "./Texts";
import contents from "../constants/contents";

const HomeButton = () => {
  const [showTooltip, setShowTooltip] = useState(false);
  const goHome = () => {
    RootNavigation.navigate("Home");
  };
  return (
    <View style={styles.positioning}>
      {showTooltip ? (
        <View style={styles.tooltip}>
          <Body
            content={contents.label.goToHomeButton}
            color={colorScheme.neutral_5}
            weight="b"
            size={12}
          />
        </View>
      ) : null}
      <TouchableOpacity
        style={styles.outer}
        onPress={goHome}
        onLongPress={() => setShowTooltip(true)}
        onPressOut={() => setShowTooltip(false)}
      >
        <View style={styles.inner}>
          <SimpleLineIcons
            name="home"
            size={30}
            color={colorScheme.neutral_5}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  positioning: {
    position: "absolute",
    bottom: 32,
    right: 18,
    zIndex: 998,
    flexDirection: "row",
    alignItems: "center",
  },
  outer: {
    width: 60,
    height: 60,
    borderRadius: 24,
    backgroundColor: "rgba(223, 223, 223, 0.24)",
    borderColor: "#FFF",
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    overflow: "visible",
  },
  inner: {
    width: 50,
    height: 50,
    borderRadius: 20,
    backgroundColor: colorScheme.primary,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: colorScheme.neutral_1,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 10,
    elevation: 10,
  },
  tooltip: {
    backgroundColor: colorScheme.neutral_1,
    borderRadius: 8,
    paddingVertical: 4,
    paddingHorizontal: 18,
    margin: 4,
    shadowColor: colorScheme.neutral_1,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 12,
    elevation: 10,
  },
});

export default HomeButton;
