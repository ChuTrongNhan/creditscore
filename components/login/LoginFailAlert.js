import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Card, Icon } from "@ui-kitten/components";
import { Body } from "../Texts";
import contents from "../../constants/contents";
import colorScheme from "../../constants/color-scheme";

const LoginFailAlert = ({ status, onCancel }) => {
  const Footer = () => {
    return (
      <TouchableOpacity style={styles.header} onPress={onCancel}>
        <Body
          content={contents.label.ok}
          size={16}
          weight="b"
          color={colorScheme.primary}
        />
      </TouchableOpacity>
    );
  };

  return (
    <Card footer={Footer}>
      <View style={styles.body}>
        <Icon
          style={{ width: 28, height: 28 }}
          fill={colorScheme.primary}
          name="alert-triangle-outline"
        />
        <Body
          content={contents.other.loginFailAlert[status]}
          color={colorScheme.neutral_1}
          style={{ textAlign: "center", marginTop: 8 }}
        />
      </View>
    </Card>
  );
};

const styles = StyleSheet.create({
  header: {
    paddingVertical: 18,
    alignItems: "center",
    justifyContent: "center",
  },
  body: {
    padding: 8,
    alignItems: "center",
  },
});

export default LoginFailAlert;
