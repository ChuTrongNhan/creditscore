import React, { useState } from "react";
import { TextInput } from "react-native";
import appStyle from "./Styles";

export const Input = ({
  placeholder,
  keyboardType,
  value,
  secure,
  onChangeText,
  style,
}) => {
  const [focus, setFocus] = useState(false);
  return (
    <TextInput
      keyboardType={keyboardType ? keyboardType : "default"}
      placeholder={placeholder}
      value={value}
      secureTextEntry={secure ? secure : false}
      style={
        focus
          ? { ...appStyle.textInput, ...appStyle.textInputFocus, ...style }
          : { ...appStyle.textInput, ...style }
      }
      onChangeText={onChangeText ? (text) => onChangeText(text) : () => {}}
      onFocus={() => setFocus(true)}
      onBlur={() => setFocus(false)}
    />
  );
};

export const InputPhoneNumber = ({
  placeholder,
  value,
  onChangeText,
  style,
}) => {
  const [focus, setFocus] = useState(false);
  return (
    <TextInput
      keyboardType="phone-pad"
      placeholder={placeholder}
      value={value}
      style={
        focus
          ? { ...appStyle.phoneInput, ...appStyle.textInputFocus, ...style }
          : { ...appStyle.phoneInput, ...style }
      }
      onChangeText={onChangeText ? (text) => onChangeText(text) : () => {}}
      onFocus={() => setFocus(true)}
      onBlur={() => setFocus(false)}
    />
  );
};
