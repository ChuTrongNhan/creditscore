import React from "react";
import { StyleSheet, View } from "react-native";
import { Spinner } from "@ui-kitten/components";
import layout from "../constants/Layout";
import colorScheme from "../constants/color-scheme";

const Loader = ({ active }) => {
  if (!active) return null;
  else
    return (
      <View style={styles.container}>
        <View direction="alternate">
          <Spinner size="giant" />
        </View>
      </View>
    );
};

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    zIndex: 999,
    top: 0,
    height: layout.window.height,
    width: layout.window.width,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colorScheme.backdrop,
  },
});

export default Loader;
