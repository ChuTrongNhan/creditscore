import React from "react";
import { View, StyleSheet } from "react-native";
import { Body } from "../Texts";
import contents from "../../constants/contents";
import { Icon } from "@ui-kitten/components";
import colorScheme from "../../constants/color-scheme";
import appStyle from "../Styles";
import layout from "../../constants/Layout";
import theme from "../call-history/ChartTheme";
import { VictoryChart, VictoryArea, VictoryBar } from "victory-native";

import * as Animatable from "react-native-animatable";

const CommunityCard = ({ community }) => {
  return (
    <Animatable.View animation="fadeInUp" style={styles.container}>
      <View style={styles.cardHead}>
        <Icon
          style={appStyle.icon}
          fill={colorScheme.neutral_2}
          name="people-outline"
        />
        <Body
          content={contents.label.community}
          style={styles.cardTitle}
          color={colorScheme.neutral_0}
          weight="b"
          size={14}
        />
      </View>
      <View style={styles.creditFactorCard}>
        {community.length > 0 ? (
          <>
            <Body
              content="%"
              color={colorScheme.neutral_2}
              size={12}
              style={{ position: "absolute", top: 10, left: 32 }}
            />
            <VictoryChart width={layout.window.width - 32 * 2} theme={theme}>
              <VictoryBar
                domain={{ x: [250, 900] }}
                style={{
                  data: {
                    fill: ({ index }) => colorScheme.chart_bar[index],
                  },
                }}
                data={community}
                barRatio={1.2}
              />
            </VictoryChart>
          </>
        ) : (
          <View style={{ alignItems: "center" }}>
            <Icon
              style={appStyle.iconSmall}
              fill={colorScheme.neutral_3}
              name="alert-circle"
            />
            <Body
              content={contents.other.noCallHistoryChartData}
              size={14}
              color={colorScheme.neutral_2}
              style={{ textAlign: "center", marginTop: 8 }}
            />
          </View>
        )}
      </View>
    </Animatable.View>
  );
};

const styles = StyleSheet.create({
  container: {},
  creditFactorCard: {
    backgroundColor: colorScheme.neutral_5,
    borderRadius: 18,
    marginBottom: 36,
    padding: 18,
  },
  cardHead: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 8,
  },
  cardTitle: {
    textTransform: "uppercase",
    paddingLeft: 8,
  },
});

export default CommunityCard;
