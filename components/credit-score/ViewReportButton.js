import React from "react";
import { TouchableOpacity, StyleSheet, View } from "react-native";
import colorScheme from "../../constants/color-scheme";
import contents from "../../constants/contents";
import { Body } from "../Texts";
import { Icon } from "@ui-kitten/components";
import appStyle from "../Styles";

const ViewReportButton = ({ onViewReport }) => {
  return (
    <TouchableOpacity style={styles.viewReportButton} onPress={onViewReport}>
      <Body
        content={contents.label.viewrReport}
        color={colorScheme.primaryDarker}
        weight="m"
        style={{ paddingRight: 8 }}
      />
      <Icon
        style={appStyle.icon}
        fill={colorScheme.primaryDarker}
        name="clipboard-outline"
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  viewReportButton: {
    flexDirection: "row",
    borderRadius: 8,
    height: 48,
    paddingHorizontal: 18,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 36,
    borderColor: colorScheme.primary,
    borderWidth: 1,
  },
});

export default ViewReportButton;
