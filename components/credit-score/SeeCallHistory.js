import React from "react";
import { TouchableOpacity, StyleSheet, View } from "react-native";
import colorScheme from "../../constants/color-scheme";
import contents from "../../constants/contents";
import { Body } from "../Texts";
import { Icon } from "@ui-kitten/components";
import appStyle from "../Styles";

const SeeCallHistory = ({ onSeeCallHistory }) => {
  return (
    <TouchableOpacity
      style={styles.callHistoryButton}
      onPress={onSeeCallHistory}
    >
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Body
          content={contents.label.seeCallHistory}
          color={colorScheme.neutral_5}
          weight="b"
          style={{ paddingRight: 8 }}
        />
        <Icon
          style={appStyle.icon}
          fill={colorScheme.neutral_5}
          name="phone-call"
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  callHistoryButton: {
    backgroundColor: colorScheme.primary,
    borderRadius: 8,
    height: 72,
    paddingHorizontal: 18,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 36,
    //Shadow for ios
    // shadowColor: colorScheme.showdowIOS,
    // shadowOffset: {
    //   width: 0,
    //   height: 7,
    // },
    // shadowOpacity: 0.4,
    // shadowRadius: 12,
  },
});

export default SeeCallHistory;
