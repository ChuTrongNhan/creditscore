import React, { useEffect, useRef } from "react";
import { View, StyleSheet } from "react-native";

import colorScheme from "../../constants/color-scheme";
import contents from "../../constants/contents";
import layout from "../../constants/Layout";
import { Body } from "../Texts";
import SvgChart from "./SvgChart";
import SvgPointer from "./SvgPointer";

import * as Animatable from "react-native-animatable";

const CreditScoreChart = ({ score = 0, top = 0 }) => {
  const creditScoreRef = useRef(null);

  const rotation =
    score >= 300 && score <= 850
      ? -120 + (240 / 550) * (score - 300) + "deg"
      : score < 300
      ? "-120deg"
      : "120deg";

  const getRank = () => {
    if (score > 799) return contents.other.exceptionalCreditScore;
    if (score > 739) return contents.other.verygoodCreditScore;
    if (score > 669) return contents.other.goodCreditScore;
    if (score > 579) return contents.other.fairCreditScore;
    return contents.other.poorCreditScore;
  };

  const rotatePointer = {
    from: {
      transform: [{ rotate: "-120deg" }],
    },
    to: {
      transform: [{ rotate: rotation }],
    },
  };

  useEffect(() => {
    if (score > 0) creditScoreRef.current.animate("tada");
  }, [score]);

  return (
    <View style={styles.container}>
      <SvgChart size={250} />
      <Animatable.View
        animation={rotatePointer}
        style={{
          position: "absolute",
          left: 0,
          right: 0,
          alignItems: "center",
        }}
      >
        <SvgPointer size={250} />
      </Animatable.View>
      {score > 0 ? (
        <Animatable.View ref={creditScoreRef} style={styles.scoreBoard}>
          <Body
            content={score > 0 ? score : "???"}
            color={colorScheme.primary}
            size={36}
            weight="b"
          />
          <Body
            content={getRank() + " SCORE"}
            weight="b"
            size={14}
            color={colorScheme.accentDarker}
            style={{ textAlign: "center" }}
          />
        </Animatable.View>
      ) : null}
      <View style={styles.percentage}>
        <Body
          content={`Customer is in top ${top} %`}
          weight="m"
          color={colorScheme.neutral_1}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 18,
  },
  scoreBoard: {
    paddingHorizontal: 80,
    position: "absolute",
    top: -18,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: "center",
    alignItems: "center",
  },
  percentage: {
    position: "absolute",
    bottom: 18,
    left: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default CreditScoreChart;
