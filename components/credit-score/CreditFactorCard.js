import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { Body } from "../Texts";
import contents from "../../constants/contents";
import { Icon } from "@ui-kitten/components";
import colorScheme from "../../constants/color-scheme";
import appStyle from "../Styles";

import * as Animatable from "react-native-animatable";

const FactorRow = ({ style, factor, children }) => {
  return (
    <View style={style}>
      <View style={styles.factorRow}>
        <Body
          style={{ maxWidth: "50%" }}
          content={factor.label}
          color={colorScheme.neutral_1}
          size={14}
          weight="r"
        />
        <Body
          style={{ maxWidth: "50%", textAlign: "right" }}
          content={factor.value + " " + contents.label.VNcurrency}
          color={colorScheme.primaryDarker}
          size={14}
          weight="b"
        />
      </View>
      {children}
    </View>
  );
};

const CreditFactorCard = ({ factorList = [] }) => {
  return (
    <Animatable.View animation="fadeInUp">
      <View style={styles.cardHead}>
        <Icon
          style={appStyle.icon}
          fill={colorScheme.neutral_2}
          name="bar-chart"
        />
        <Body
          content={contents.label.creditFactor}
          style={styles.cardTitle}
          color={colorScheme.neutral_0}
          weight="b"
          size={14}
        />
      </View>
      <View style={styles.creditFactorCard}>
        {factorList.map((factor) => {
          // if (factor.label != contents.label.deposit)
          return (
            <FactorRow
              key={factor.label}
              factor={factor}
              style={
                factor.label != contents.label.deposit
                  ? {}
                  : styles.borderVertical
              }
            />
          );
          // else
          //   return (
          //     <FactorRow
          //       style={styles.borderVertical}
          //       key={factor.label}
          //       factor={factor}
          //     >
          //       {factor.value === "0" ? null : (
          //         <TouchableOpacity
          //           style={{
          //             flexDirection: "row",
          //             justifyContent: "space-between",
          //           }}
          //         >
          //           <Body
          //             content={contents.other.viewDepositChart}
          //             color={colorScheme.accent}
          //             size={14}
          //             weight="r"
          //             style={{
          //               textAlign: "right",
          //               marginBottom: 18,
          //             }}
          //           />
          //           <Icon
          //             style={appStyle.iconSmall}
          //             fill={colorScheme.accent}
          //             name="arrow-forward"
          //           />
          //         </TouchableOpacity>
          //       )}
          //     </FactorRow>
          //   );
        })}
      </View>
    </Animatable.View>
  );
};

const styles = StyleSheet.create({
  creditFactorCard: {
    backgroundColor: colorScheme.neutral_5,
    borderRadius: 18,
    paddingHorizontal: 24,
    paddingVertical: 8,
    marginBottom: 36,
  },
  cardHead: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 8,
  },
  cardTitle: {
    textTransform: "uppercase",
    paddingLeft: 8,
  },
  factorRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 18,
    alignItems: "center",
  },
  borderVertical: {
    borderTopColor: colorScheme.neutral_4,
    borderTopWidth: 1,
    borderBottomColor: colorScheme.neutral_4,
    borderBottomWidth: 1,
  },
});

export default CreditFactorCard;
