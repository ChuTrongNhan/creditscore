import React from "react";
import Svg, { Path } from "react-native-svg";

const SvgPointer = ({ size, rotate }) => {
  return (
    <Svg width={size} height={size} viewBox="0 0 530 530" fill="none">
      <Path d="M264.5 53l26.414 45.75h-52.828L264.5 53z" fill="#7553BC" />
    </Svg>
  );
};

export default SvgPointer;
