import React, { useState } from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { Body } from "../Texts";
import contents from "../../constants/contents";
import { Icon } from "@ui-kitten/components";
import colorScheme from "../../constants/color-scheme";
import appStyle from "../Styles";

import * as Animatable from "react-native-animatable";

const RecentCallCard = ({ recentCalls = [], onSeeDetail = () => {} }) => {
  const [showFull, setShowFull] = useState(false);
  const previewLength = 3;
  const getRank = (score) => {
    if (score > 799) return contents.other.exceptionalCreditScore;
    if (score > 739) return contents.other.verygoodCreditScore;
    if (score > 669) return contents.other.goodCreditScore;
    if (score > 579) return contents.other.fairCreditScore;
    return contents.other.poorCreditScore;
  };
  return (
    <Animatable.View animation="fadeInUp">
      <View style={styles.cardHead}>
        <Icon
          style={appStyle.icon}
          fill={colorScheme.neutral_2}
          name="clock-outline"
        />
        <Body
          content={contents.label.recentCall}
          style={styles.cardTitle}
          color={colorScheme.neutral_0}
          weight="b"
          size={14}
        />
      </View>
      <View style={styles.recentCallCard}>
        {recentCalls.map((recentCall, index) => {
          if (!showFull && index > previewLength - 1) return null;
          else
            return (
              <Animatable.View
                key={recentCall.phone}
                animation="fadeInUp"
                easing="linear"
                duration={index * 150}
              >
                <TouchableOpacity
                  style={styles.recentCall}
                  onPress={() => {
                    if (recentCall.credit_score > 0) onSeeDetail(recentCall);
                  }}
                >
                  <Body
                    content={recentCall.phone}
                    color={colorScheme.neutral_1}
                  />
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <View
                      style={{
                        justifyContent: "center",
                        alignItems: "flex-end",
                      }}
                    >
                      {recentCall.credit_score > 0 ? (
                        <>
                          <Body
                            content={recentCall.credit_score}
                            size={16}
                            weight="b"
                          />
                          <Body
                            content={getRank(recentCall.credit_score)}
                            color={colorScheme.primaryDarker}
                            size={12}
                            weight="m"
                          />
                        </>
                      ) : (
                        <Body
                          content={contents.other.noData}
                          color={colorScheme.neutral_2}
                          size={14}
                          weight="m"
                        />
                      )}
                    </View>
                    <Icon
                      style={{
                        ...appStyle.icon,
                        opacity: recentCall.credit_score > 0 ? 1 : 0,
                      }}
                      fill={colorScheme.primary}
                      name="chevron-right"
                    />
                  </View>
                </TouchableOpacity>
              </Animatable.View>
            );
        })}
        <TouchableOpacity onPress={() => setShowFull(!showFull)}>
          <View style={styles.bottomButton}>
            <Body
              content={
                showFull
                  ? contents.label.seeLessRecentCall
                  : contents.label.seeMoreRecentCall
              }
              color={colorScheme.primaryDarker}
              weight="b"
              size={14}
            />
          </View>
        </TouchableOpacity>
      </View>
    </Animatable.View>
  );
};

const styles = StyleSheet.create({
  recentCallCard: {
    backgroundColor: colorScheme.neutral_5,
    borderRadius: 18,
    padding: 8,
    paddingTop: 4,
    paddingBottom: 0,
    marginBottom: 36,
  },
  cardHead: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 8,
  },
  cardTitle: {
    textTransform: "uppercase",
    paddingLeft: 8,
  },
  recentCall: {
    backgroundColor: colorScheme.neutral_4,
    borderRadius: 8,
    padding: 12,
    paddingRight: 2,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 4,
  },
  bottomButton: {
    height: 60,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default RecentCallCard;
