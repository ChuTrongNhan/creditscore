import React from "react";
import { View, StyleSheet } from "react-native";
import { Body } from "../Texts";
import colorScheme from "../../constants/color-scheme";
import { Icon } from "@ui-kitten/components";
import CreditScoreChart from "./CreditScoreChart";
import appStyle from "../Styles";

const Compare = ({ goUp, amount, compareTo }) => {
  return (
    <View style={styles.compare}>
      <View style={styles.compareValue}>
        {goUp ? (
          <Icon
            style={appStyle.icon}
            fill={colorScheme.primary}
            name="arrow-upward-outline"
          />
        ) : (
          <Icon
            style={appStyle.icon}
            fill={colorScheme.primary}
            name="arrow-downward-outline"
          />
        )}

        <Body
          content={amount}
          color={colorScheme.primary}
          size={36}
          weight="b"
        />
      </View>
      <Body content={compareTo} color={colorScheme.neutral_1} size={14} />
    </View>
  );
};

const CreditScoreCard = ({
  score = 0,
  lastMonth = 0,
  lastWeek = 0,
  top = 0,
}) => {
  return (
    <View style={styles.creditScoreCard}>
      <CreditScoreChart score={score} top={top} />
      <View style={styles.compares}>
        <Compare
          goUp={score >= lastWeek}
          amount={Math.abs(score - lastWeek)}
          compareTo={contents.label.lastWeek}
        />
        <Compare
          goUp={score >= lastMonth}
          amount={Math.abs(score - lastMonth)}
          compareTo={contents.label.lastMonth}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  creditScoreCard: {
    alignItems: "center",
    backgroundColor: colorScheme.neutral_5,
    borderRadius: 18,
    padding: 18,
    marginBottom: 36,
  },
  compares: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  compare: {
    position: "relative",
    alignItems: "center",
  },
  compareValue: {
    flexDirection: "row",
    paddingRight: 18,
  },
});

export default CreditScoreCard;
