export default colorScheme = {
  primary: "#09CABA",
  primaryDarker: "#05BDBA",
  primaryLighter: "#8BE8E0",

  accent: "#7553BC",
  accentDarker: "#462076",
  accentLighter: "#9C7CD6",

  neutral_0: "#3D4E55",
  neutral_1: "#677276",
  neutral_2: "#C0C0C0",
  neutral_3: "#DFDFDF",
  neutral_4: "#F6F6F6",
  neutral_5: "#FFFFFF",

  // neutral_5: "#353535",
  // neutral_4: "#444",
  // neutral_3: "#C0C0C0",
  // neutral_2: "#DFDFDF",
  // neutral_1: "#F6F6F6",
  // neutral_0: "#FFFFFF",

  backdrop: "rgba(50, 50, 50, 0.5)",
  shadowIOS: "#09CABA",

  chart_bar: [
    "#A0E9DD",
    "#85E5D8",
    "#6DE1D4",
    "#54DDCF",
    "#3AD9CB",
    "#24D6C6",
    "#27C5C4",
    "#35B1C2",
    "#439BC1",
    "#5187BF",
    "#626FBE",
    "#7454BC",
  ],
};
