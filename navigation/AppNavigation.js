import React, { useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { navigationRef } from "./RootNavigation";

import Login from "../screens/Login.screen";
import Home from "../screens/Home.screen";
import CreditScore from "../screens/CreditScore.screen";
import CreditReport from "../screens/CreditReport.screen";
import CallHistory from "../screens/CallHistory.screen";

const AppStack = createStackNavigator();

const AppNavigation = (props) => {
  return (
    <NavigationContainer ref={navigationRef}>
      <AppStack.Navigator
        screenOptions={{ headerShown: false, gestureEnabled: false }}
      >
        <AppStack.Screen name="Login" component={Login} />
        <AppStack.Screen name="Home" component={Home} />
        <AppStack.Screen name="CreditScore" component={CreditScore} />
        <AppStack.Screen name="CreditReport" component={CreditReport} />
        <AppStack.Screen name="CallHistory" component={CallHistory} />
      </AppStack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigation;
